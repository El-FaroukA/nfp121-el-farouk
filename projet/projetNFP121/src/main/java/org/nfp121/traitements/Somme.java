package org.nfp121.traitements;

/**
  * org.nfp121.traitements.Somme calcule la sommee des valeurs, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class Somme extends SommeAbstrait {

	private double somme = 0.0;

	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		this.somme += valeur;
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
	}

	@Override
	public double somme() {
		return this.somme;
	}
}
