package org.nfp121.traitements;

import java.util.Map;

/**
  * org.nfp121.traitements.SommeParPosition
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {

	private final Map<Position, Double> sommeParPosition;

    public SommeParPosition() {
        this.sommeParPosition = new java.util.HashMap<>();
    }

    @Override
    public void traiter(Position p, double valeur) {
        this.sommeParPosition.merge(p, valeur, Double::sum);
        super.traiter(p, valeur);
    }

    @Override
    public void gererFinLotLocal(String nomLot) {
        System.out.println("SommeParPosition " + nomLot + " :");
        for (Map.Entry<Position, Double> entry : this.sommeParPosition.entrySet()) {
            System.out.println("- " + entry.getKey() + " -> " + entry.getValue());
        }
        System.out.println("Fin SommeParPosition.");
    }

}
