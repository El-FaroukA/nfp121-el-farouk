package org.nfp121.traitements;

/**
  * org.nfp121.traitements.SupprimerPlusGrand supprime les valeurs plus grandes qu'un seuil.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusGrand extends Traitement {

    private final double seuil;

    public SupprimerPlusGrand(double seuil) {
        this.seuil = seuil;
    }

    @Override
    public void traiter(Position position, double valeur) {
        if (valeur <= this.seuil) {
            super.traiter(position, valeur);
        }
    }
}
