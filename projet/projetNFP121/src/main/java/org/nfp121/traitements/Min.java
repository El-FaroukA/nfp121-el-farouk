package org.nfp121.traitements;

/**
 * org.nfp121.traitements.Max calcule le max des valeurs vues, quelque soit le lot.
 *
 * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
 */

public class Min extends Traitement {

    private double min = Double.POSITIVE_INFINITY;

    @Override
    public void traiter(Position position, double valeur) {
        if (valeur < this.min) {
            this.min = valeur;
        }
        super.traiter(position, valeur);
    }

    @Override
    public void gererFinLotLocal(String nomLot) {
        System.out.println(nomLot + " : min = " + this.min);
    }

    public double getMin() {
        return this.min;
    }

}
