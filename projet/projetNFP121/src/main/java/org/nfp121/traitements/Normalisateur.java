package org.nfp121.traitements;

import java.util.ArrayList;
import java.util.List;
/**
  * org.nfp121.traitements.Normalisateur normalise les donnÃ©es d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Normalisateur extends Traitement {

    private final double debut;
    private final double fin;
    private final Max max;
    private final Min min;
    private final List<Double> valeurs;

    public Normalisateur(double debut, double fin) {
        this.debut = debut;
        this.fin = fin;
        this.max = new Max();
        this.min = new Min();
        this.valeurs = new ArrayList<>();
    }

    @Override
    public void traiter(Position position, double valeur) {
        this.valeurs.add(valeur);
        this.max.traiter(position, valeur);
        this.min.traiter(position, valeur);
    }

    @Override
    public void gererDebutLotLocal(String nomLot) {
        this.max.gererDebutLot(nomLot);
        this.min.gererDebutLot(nomLot);
        this.valeurs.clear();
    }

    @Override
    public void gererFinLotLocal(String nomLot) {
        this.max.gererFinLot(nomLot);
        this.min.gererFinLot(nomLot);

        double minVal = this.min.getMin();
        double maxVal = this.max.getMax();

        double a = (fin - debut) / (maxVal - minVal);
        double b = fin - a * maxVal;

        for (Double valeur : valeurs) {
            double valeurNorm = a * valeur + b;
            System.out.println("Normalisation de " + valeur + " en " + valeurNorm);
            Position position = new Position(0, 0);
            super.traiter(position, valeurNorm);
        }

        System.out.println("Normalisation du lot " + nomLot + " terminée.");
    }
}

