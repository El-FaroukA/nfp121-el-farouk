package org.nfp121.traitements;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

/**
 * org.nfp121.traitements.GenerateurXML
 *
 * Produit à chaque fin de lot, un document XML avec toutes les données traitées (de ce lot et des précédents) organisées par lot.
 * Le nom du fichier dans lequel le document sera écrit est un paramètre du constructeur de ce traitement.
 *
 * @autor Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {

    private final String nomFichier;
    private final Map<String, Donnees> donneesParLot;
    private final Donnees toutesDonnees;
    private String nomLot;

    public GenerateurXML(String nomFichier) {
        this.nomFichier = "src/test/resources/expected_xml/" + nomFichier;
        this.donneesParLot = new HashMap<>();
        this.toutesDonnees = new Donnees();
    }

    @Override
    public void traiter(Position position, double valeur) {
        if (nomLot != null) {
            Donnees donneesLot = donneesParLot.getOrDefault(nomLot, new Donnees());
            donneesLot.traiter(position, valeur);
            donneesParLot.put(nomLot, donneesLot);
            toutesDonnees.traiter(position, valeur);
        }
        super.traiter(position, valeur);
    }

    @Override
    public void gererDebutLotLocal(String nomLot) {
        this.nomLot = nomLot;
    }

    @Override
    public void gererFinLotLocal(String nomLot) {
        try {
            // Création du document XML
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            DOMImplementation domImpl = documentBuilder.getDOMImplementation();
            DocumentType doctype = domImpl.createDocumentType("lots", null, "generateur.dtd");
            Document document = domImpl.createDocument(null, "lots", doctype);

            Element racine = document.getDocumentElement();

            // Ajout des lots et des données
            for (Map.Entry<String, Donnees> entry : donneesParLot.entrySet()) {
                Element lotElement = document.createElement("lot");
                lotElement.setAttribute("nom", entry.getKey());
                racine.appendChild(lotElement);

                for (Map.Entry<Position, Double> donneesEntry : entry.getValue().getDonnees().entrySet()) {
                    Element donneeElement = document.createElement("donnee");
                    donneeElement.setAttribute("x", Integer.toString(donneesEntry.getKey().getX()));
                    donneeElement.setAttribute("y", Integer.toString(donneesEntry.getKey().getY()));
                    donneeElement.setTextContent(Double.toString(donneesEntry.getValue()));
                    lotElement.appendChild(donneeElement);
                }
            }

            // Écriture du document dans un fichier
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new FileWriter(nomFichier));
            transformer.transform(domSource, streamResult);

            System.out.println("Document XML généré : " + nomFichier);

        } catch (Exception e) {
            System.err.println("Erreur lors de la génération du document XML : " + e.getMessage());
        }
    }
}