package org.nfp121.traitements;

import org.nfp121.exceptions.CycleException;

import java.util.*;

/**
  * org.nfp121.traitements.Traitement modÃ©lise un traitement et ses traitements suivants (donc
  * une chaÃ®ne de traitements).
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
abstract public class Traitement {

	/** Les traitements suivants. */
	private List<Traitement> suivants = new ArrayList<>();

	/** Ajouter des traitements Ã  la suite de celui-ci.
	 * @param suivants les traitements Ã  ajouter
	 */
	final public Traitement ajouterSuivants(Traitement... suivants) throws CycleException {
		for (Traitement suivant : suivants) {
			if (detecterCycle(suivant)) {
				throw new CycleException("Cycle detécté dans les traitements.");
			}
			this.suivants.add(suivant);
		}
		return this;
	}

	private boolean detecterCycle(Traitement nouveauTraitement) {
		Set<Traitement> traitements = new HashSet<>();
		return detecterCycleRecursif(this, nouveauTraitement, traitements);
	}

	private boolean detecterCycleRecursif(Traitement courant, Traitement nouveauTraitement, Set<Traitement> traitements) {
		if (courant == nouveauTraitement) {
			return true;
		}
		traitements.add(courant);
		for (Traitement suivant : courant.suivants) {
			if (!traitements.contains(suivant) && detecterCycleRecursif(suivant, nouveauTraitement, traitements)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public final String toString() {
		return this.toString("");
	}

	/** Afficher ce traitement et les suivants sous forme d'un arbre horizontal.
	 * @param prefixe le prÃ©fixe Ã  afficher aprÃ¨s un retour Ã  la ligne
	 */
	private String toString(String prefixe) {
		String res =  this.getClass().getName();
		String complement = this.toStringComplement();
		if (complement != null && complement.length() > 0) {
			res += "(" + complement + ")";
		}
		if (this.suivants.size() <= 1) {
			for (Traitement s : this.suivants) {
				res += " --> " + s.toString(prefixe);
			}
		} else {
			for (Traitement s : this.suivants) {
				res += "\n" + prefixe + "\t" + "--> " + s.toString(prefixe + "\t");
			}
		}
		return res;
	}

	/** DÃ©crire les paramÃ¨tres du traitement. */
	protected String toStringComplement() {
		return null;
	}

	public void traiter(Position position, double valeur) {
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

	final public void gererDebutLot(String nomLot) {
		Objects.requireNonNull(nomLot, "nomLot doit Ãªtre dÃ©fini.");

		this.gererDebutLotLocal(nomLot);
		for (Traitement suivant : this.suivants) {
			suivant.gererDebutLot(nomLot);
		}
	}

	protected void gererDebutLotLocal(String nomLot) {
	}

	final public void gererFinLot(String nomLot) {
		Objects.requireNonNull(nomLot, "nomLot doit Ãªtre dÃ©fini.");

		this.gererFinLotLocal(nomLot);
		for (Traitement suivant : this.suivants) {
			suivant.gererFinLot(nomLot);
		}
	}

	protected void gererFinLotLocal(String nomLot) {
	}

}
