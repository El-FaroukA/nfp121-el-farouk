package org.nfp121.traitements;

/**
  * org.nfp121.traitements.Multiplicateur transmet la valeur multipliÃ©e par un facteur.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Multiplicateur extends Traitement {

    private final double facteur;

    public Multiplicateur(double facteur) {
        this.facteur = facteur;
    }

	@Override
    public void traiter(Position position, double valeur) {
        double resultat = valeur * this.facteur;
        super.traiter(position, resultat);
    }

    @Override
    public String toStringComplement() {
        return "facteur = " + this.facteur;
    }


}
