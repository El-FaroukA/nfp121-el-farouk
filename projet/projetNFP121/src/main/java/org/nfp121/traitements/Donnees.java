package org.nfp121.traitements;

import java.util.List;
import java.util.Map;

/**
  * Donnees enregistre toutes les donnÃ©es reÃ§ues, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	private final Map<Position, Double> donnees;

	public Donnees() {
		this.donnees = new java.util.HashMap<>();
	}

	@Override
	public void traiter(Position p, double valeur) {
		this.donnees.put(p, valeur);
		super.traiter(p, valeur);
	}

	public Map<Position, Double> getDonnees() {
		return this.donnees;
	}

}
