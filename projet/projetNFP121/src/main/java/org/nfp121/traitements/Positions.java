package org.nfp121.traitements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
  * org.nfp121.traitements.Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Positions extends PositionsAbstrait {

    private final List<Position> positions;

    public Positions() {
        this.positions = new ArrayList<>();
    }

    @Override
    public int nombre() {
        return 0;
    }

    @Override
    public Position position(int indice) {
        return this.positions.get(indice);
    }

    @Override
    public int frequence(Position position) {
        return Collections.frequency(this.positions, position);
    }

    @Override
    public void traiter(Position position, double valeur) {
        this.positions.add(position);
        super.traiter(position, valeur);
    }

}
