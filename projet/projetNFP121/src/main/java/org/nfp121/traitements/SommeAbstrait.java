package org.nfp121.traitements;

/**
  * org.nfp121.traitements.SommeAbstrait
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

abstract public class SommeAbstrait extends Traitement {
	
	public abstract double somme();

}
