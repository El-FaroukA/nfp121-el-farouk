package org.nfp121.traitements;

/**
  * org.nfp121.traitements.Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	private double max = Double.NEGATIVE_INFINITY;

	@Override
	public void traiter(Position position, double valeur) {
		if (valeur > this.max) {
			this.max = valeur;
		}
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + " : max = " + this.max);
	}

	public double getMax() {
		return this.max;
	}

}
