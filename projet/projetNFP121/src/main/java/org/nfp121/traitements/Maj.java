package org.nfp121.traitements;

/**
 * org.nfp121.traitements.Maj indique pour chaque lot les positions mises Ã  jour (ou ajoutÃ©es)
 * lors du traitement de ce lot.
 *
 * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

        @Override
        public void traiter(Position position, double valeur) {
            super.traiter(position, valeur);
            System.out.println("Mise à jour de " + position + " : " + valeur);
        }

        @Override
        public void gererFinLotLocal(String nomLot) {
            System.out.println(nomLot + " : fin de mise à jour");
        }
}
