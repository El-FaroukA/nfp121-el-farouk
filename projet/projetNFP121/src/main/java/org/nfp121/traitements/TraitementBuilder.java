package org.nfp121.traitements;

import org.nfp121.exceptions.CycleException;

import java.lang.reflect.*;
import java.util.*;

/**
  * org.nfp121.traitements.TraitementBuilder
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class TraitementBuilder {

	/** Retourne un objet de type Class correspondant au nom en paramÃ¨tre.
	 * Exemples :
	 *   - int donne int.class
	 *   - org.nfp121.traitements.Normaliseur donne org.nfp121.traitements.Normaliseur.class
	 */
    public Class<?> analyserType(String nomType) throws ClassNotFoundException {
		return switch (nomType) {
			case "int" -> int.class;
			case "double" -> double.class;
			case "java.lang.String" -> String.class;
			default -> Class.forName("org.nfp121.traitements."+nomType);
		};
	}

	/** CrÃ©e l'objet java qui correspond au type formel en exploitant le Â« mot Â» suivant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit Ãªtre un entier et le rÃ©sulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlisÃ©s dans le projet : int, double et String.
	 */
	public static Object decoderEffectif(Class<?> formel, Scanner in) {
		in.useLocale(Locale.US); // pour lire les nombres avec un point (et non une virgule)
		if (formel == int.class) {
			return in.nextInt();
		} else if (formel == double.class) {
			return in.nextDouble();
		} else if (formel == String.class) {
			return in.next();
		} else {
			throw new RuntimeException("Type non supporté : " + formel);
		}
	}



	/** DÃ©finition de la signature, les paramÃ¨tres formels, mais aussi les paramÃ¨tres effectifs.  */
	public static class Signature {
		public Class<?>[] formels;
		public Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/** Analyser une signature pour retrouver les paramÃ¨tres formels et les paramÃ¨tres effectifs.
	 * Exemple Â« 3 double 0.0 java.lang.String xyz int -5 Â» donne
	 *   - [double.class, String.class, int.class] pour les paramÃ¨tres formels et
	 *   - [0.0, "xyz", -5] pour les paramÃ¨tres effectifs.
	 */
    public Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		List<Class<?>> formels = new ArrayList<>();
		List<Object> effectifs = new ArrayList<>();

		while (in.hasNext()) {
			String token = in.next();
			// Vérifier si le token est un type formel
			if (token.equalsIgnoreCase("int") || token.equalsIgnoreCase("double") || token.equalsIgnoreCase("java.lang.String")) {
				Class<?> type = analyserType(token);
				// Vérifier si le token suivant est du bon type et le lire en utilisant decoderEffectif
				if (in.hasNext()) {
					formels.add(type);
					effectifs.add(decoderEffectif(type, in));
				} else {
					throw new RuntimeException("Erreur : le type formel " + token + " n'a pas de paramètre effectif.");
				}
			}
		}

		return new Signature(formels.toArray(new Class<?>[0]), effectifs.toArray());
	}


	/** Analyser la crÃ©ation d'un objet.
	 * Exemple : Â« org.nfp121.traitements.Normaliseur 2 double 0.0 double 100.0 Â» consiste Ã  charger
	 * la classe org.nfp121.traitements.Normaliseur, trouver le constructeur qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme paramÃ¨tres effectifs.
	 */
	Object analyserCreation(Scanner in)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException
	{
		String nomClasse = in.next();
		Class<?> classe = analyserType(nomClasse);
		Signature signature = analyserSignature(in);
		Constructor<?> constructeur = classe.getConstructor(signature.formels);
		return constructeur.newInstance(signature.effectifs);
	}


	/** Analyser un traitement.
	 * Exemples :
	 *   - Â« org.nfp121.traitements.Somme 0 0 Â»
	 *   - Â« org.nfp121.traitements.SupprimerPlusGrand 1 double 99.99 0 Â»
	 *   - Â« org.nfp121.traitements.Somme 0 1 org.nfp121.traitements.Max 0 0 Â»
	 *   - Â« org.nfp121.traitements.Somme 0 2 org.nfp121.traitements.Max 0 0 org.nfp121.traitements.SupprimerPlusGrand 1 double 20.0 0 Â»
	 *   - Â« org.nfp121.traitements.Somme 0 2 org.nfp121.traitements.Max 0 0 org.nfp121.traitements.SupprimerPlusGrand 1 double 20.0 1 org.nfp121.traitements.Positions 0 0 Â»
	 * @param in le scanner Ã  utiliser
	 * @param env l'environnement oÃ¹ enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env)
            throws ClassNotFoundException, InvocationTargetException,
            IllegalAccessException, NoSuchMethodException,
            InstantiationException, CycleException {

		env = env == null ? new HashMap<>() : env;
		List<Traitement> traitementsCrees = new ArrayList<>();

		while (in.hasNext()) {
			String nomClasse = in.next();
			// Vérifier si le token commence par une majuscule
			boolean isUpperCase = Character.isUpperCase(nomClasse.charAt(0));
			if (isUpperCase) {
				int param = in.nextInt();
				// Construire la chaîne de création pour le traitement
				StringBuilder creation = new StringBuilder(nomClasse + " " + param);
				if (param > 0) {
					for (int i = 0; i < param * 2; i++) {
						if (in.hasNext()) {
							creation.append(" ").append(in.next());
						}
					}
				}
				// Créer une instance de Traitement
				Traitement traitement = (Traitement) analyserCreation(new Scanner(creation.toString()));
				traitementsCrees.add(traitement);
				// Ajouter le traitement créé à l'environnement
				env.put(nomClasse, traitement);
			}
		}
		// Chaînage des traitements
		for (int i = 0; i < traitementsCrees.size() - 1; i++) {
			traitementsCrees.get(0).ajouterSuivants(traitementsCrees.get(i + 1));
		}
		// Retourner le premier traitement
		return traitementsCrees.isEmpty() ? null : traitementsCrees.get(0);
	}

	/** Analyser un traitement.
	 * @param in le scanner Ã  utiliser
	 * @param env l'environnement oÃ¹ enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env)
	{
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}

}
