package org.nfp121.fabrique;

import org.nfp121.traitements.Somme;
import org.nfp121.traitements.Positions;
import org.nfp121.traitements.Donnees;
import org.nfp121.traitements.Multiplicateur;
import org.nfp121.traitements.SommeParPosition;
import org.nfp121.traitements.SupprimerPlusGrand;
import org.nfp121.traitements.SupprimerPlusPetit;
import org.nfp121.traitements.Max;
import org.nfp121.traitements.Min;
import org.nfp121.traitements.Normalisateur;
import org.nfp121.traitements.GenerateurXML;
import org.nfp121.traitements.Maj;

/**
  * org.nfp121.fabrique.FabriqueTraitementConcrete concrÃ©tise org.nfp121.fabrique.FabriqueTraitement.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class FabriqueTraitementConcrete implements FabriqueTraitement {

	@Override public Somme somme() {
		return new Somme();
	}

	@Override public Positions positions() {
		return new Positions();
	}

	@Override public Donnees donnees() {
		return new Donnees();
	}

	@Override public Multiplicateur multiplicateur(double facteur) {
		return new Multiplicateur(facteur);
	}

	@Override public SommeParPosition sommeParPosition() {
		return new SommeParPosition();
	}

	@Override public SupprimerPlusGrand supprimerPlusGrand(double seuil) {
		return new SupprimerPlusGrand(seuil);
	}

	@Override public SupprimerPlusPetit supprimerPlusPetit(double seuil) {
		return new SupprimerPlusPetit(seuil);
	}

	@Override public Max max() {
		return new Max();
	}

	@Override public Min min() {
		return new Min();
	}

	@Override public Normalisateur normalisateur(double debut, double fin) {
		return new Normalisateur(debut, fin);
	}

	@Override public GenerateurXML generateurXML(String nomFichier) {
		return new GenerateurXML(nomFichier);
	}

	@Override public Maj maj() {
		return new Maj();
	}


}
