package org.nfp121.fabrique;

import org.nfp121.traitements.Donnees;
import org.nfp121.traitements.Multiplicateur;
import org.nfp121.traitements.PositionsAbstrait;
import org.nfp121.traitements.SommeAbstrait;
import org.nfp121.traitements.SommeParPosition;
import org.nfp121.traitements.SupprimerPlusGrand;
import org.nfp121.traitements.SupprimerPlusPetit;
import org.nfp121.traitements.Max;
import org.nfp121.traitements.Min;
import org.nfp121.traitements.Normalisateur;
import org.nfp121.traitements.GenerateurXML;
import org.nfp121.traitements.Maj;

/**
  * org.nfp121.fabrique.FabriqueTraitement permet de construire les traitments demandÃ©s.
  * Cette classe peut Ãªtre complÃ©tÃ©e si nÃ©cessaire.
  * Son seul intÃ©rÃªt est de pouvoir Ã©crire les classes fournies sans
  * qu'elles provoquent des erreurs.  Les erreurs seront pour l'essentiel
  * localisÃ©es dans org.nfp121.fabrique.FabriqueTraitementConcrete.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public interface FabriqueTraitement {

	SommeAbstrait somme();
	PositionsAbstrait positions();
	Donnees donnees();
	Multiplicateur multiplicateur(double facteur);
	SommeParPosition sommeParPosition();
	SupprimerPlusGrand supprimerPlusGrand(double seuil);
	SupprimerPlusPetit supprimerPlusPetit(double seuil);
	Max max();
	Min min();
	Normalisateur normalisateur(double debut, double fin);
	GenerateurXML generateurXML(String nomFichier);
	Maj maj();

}
