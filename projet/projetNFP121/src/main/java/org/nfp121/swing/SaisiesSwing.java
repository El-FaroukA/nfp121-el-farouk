package org.nfp121.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SaisiesSwing permet de saisir des données (Abscisse, Ordonnée, Valeur) via une interface graphique Swing.
 */
public class SaisiesSwing extends JFrame {

    private final JTextField abscisseField;
    private final JTextField ordonneeField;
    private final JTextField valeurField;
    private final List<String> donnees;
    private int numeroOrdre;

    public SaisiesSwing(String titre, String nomFichier) {
        super(titre);

        this.donnees = new ArrayList<>();
        this.numeroOrdre = 1;

        // Initialiser les champs de saisie
        abscisseField = new JTextField(10);
        ordonneeField = new JTextField(10);
        valeurField = new JTextField(10);

        JButton validerButton = new JButton("Valider");
        JButton effacerButton = new JButton("Effacer");
        JButton terminerButton = new JButton("Terminer");

        validerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validerSaisie();
            }
        });

        effacerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                effacerSaisie();
            }
        });

        terminerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                terminerSaisie(nomFichier);
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);

        // Disposition en colonne
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(new JLabel("Abscisse:"), gbc);

        gbc.gridx = 1;
        add(abscisseField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        add(new JLabel("Ordonnée:"), gbc);

        gbc.gridx = 1;
        add(ordonneeField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        add(new JLabel("Valeur:"), gbc);

        gbc.gridx = 1;
        add(valeurField, gbc);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        buttonPanel.add(validerButton);
        buttonPanel.add(effacerButton);
        buttonPanel.add(terminerButton);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        add(buttonPanel, gbc);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void validerSaisie() {
        try {
            int abscisse = Integer.parseInt(abscisseField.getText());
            int ordonnee = Integer.parseInt(ordonneeField.getText());
            double valeur = Double.parseDouble(valeurField.getText());

            String donnee = numeroOrdre + " " + abscisse + " " + ordonnee + " " + valeur;
            donnees.add(donnee);
            numeroOrdre++;

            abscisseField.setBackground(Color.WHITE);
            ordonneeField.setBackground(Color.WHITE);
            valeurField.setBackground(Color.WHITE);

            effacerSaisie();
        } catch (NumberFormatException e) {
            if (!isInteger(abscisseField.getText())) {
                abscisseField.setBackground(Color.RED);
            } else {
                abscisseField.setBackground(Color.WHITE);
            }

            if (!isInteger(ordonneeField.getText())) {
                ordonneeField.setBackground(Color.RED);
            } else {
                ordonneeField.setBackground(Color.WHITE);
            }

            if (!isDouble(valeurField.getText())) {
                valeurField.setBackground(Color.RED);
            } else {
                valeurField.setBackground(Color.WHITE);
            }
        }
    }

    private void effacerSaisie() {
        abscisseField.setText("");
        ordonneeField.setText("");
        valeurField.setText("");

        abscisseField.setBackground(Color.WHITE);
        ordonneeField.setBackground(Color.WHITE);
        valeurField.setBackground(Color.WHITE);
    }

    private void terminerSaisie(String nomFichier) {
        try (FileWriter writer = new FileWriter(nomFichier)) {
            for (String donnee : donnees) {
                writer.write(donnee + "\n");
            }
        } catch (IOException e) {
            System.err.println("Erreur lors de l'écriture du fichier : " + e.getMessage());
        }
        System.exit(0);
    }

    private boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isDouble(String text) {
        try {
            Double.parseDouble(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        String nomFichier = "donnees.txt";

        if (args.length > 0) {
            nomFichier = args[0];
        }

        new SaisiesSwing("Saisie de données", nomFichier);
    }
}
