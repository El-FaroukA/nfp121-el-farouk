package org.nfp121;

import org.nfp121.analyseur.Analyseur;
import org.nfp121.traitements.*;

import java.io.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import static java.util.AbstractMap.SimpleImmutableEntry;

public class Main {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java org.nfp121.Main <path to XML data file> <path to treatment file>");
            System.exit(1);
        }

        String xmlFilePath = args[0];
        String treatmentFilePath = args[1];

        // Lire les positions à partir du fichier XML
        List<SimpleImmutableEntry<Position, Double>> data = readDataFromXML(xmlFilePath);

        // Lire les traitements à partir du fichier texte
        Traitement traitement = readTreatmentFromFile(treatmentFilePath);

        // Créer l'analyseur et traiter les données
        Analyseur analyseur = new Analyseur(traitement);
        analyseur.traiter(data, "Lot1");

        System.out.println("Traitement terminé.");
    }

    private static List<AbstractMap.SimpleImmutableEntry<Position, Double>> readDataFromXML(String filePath) {
        List<AbstractMap.SimpleImmutableEntry<Position, Double>> data = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);
            factory.setNamespaceAware(true);

            DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setEntityResolver((publicId, systemId) -> {
                if (systemId.contains("donnees2.dtd")) {
                    try {
                        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                        InputStream is = classloader.getResourceAsStream("donnees2.dtd");
                        return new InputSource(is);
                    } catch (Exception e) {
                        throw new SAXException("Erreur lors du chargement de la DTD", e);
                    }
                } else {
                    return null;
                }
            });

            builder.setErrorHandler(new DefaultHandler() {
                @Override
                public void fatalError(SAXParseException e) throws SAXException {
                    throw e;
                }

                @Override
                public void error(SAXParseException e) throws SAXException {
                    throw e;
                }

                @Override
                public void warning(SAXParseException e) throws SAXException {
                    System.err.println("Warning: " + e.getMessage());
                }
            });

            Document document = builder.parse(new File(filePath));
            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("donnee");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    int x = Integer.parseInt(element.getAttribute("x"));
                    int y = Integer.parseInt(element.getAttribute("y"));
                    double valeur = Double.parseDouble(element.getElementsByTagName("valeur").item(0).getTextContent());
                    Position position = new Position(x, y);
                    data.add(new AbstractMap.SimpleImmutableEntry<>(position, valeur));
                }
            }
        } catch (SAXException e) {
            System.err.println("Le fichier XML ne respecte pas la DTD : " + e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            System.err.println("Erreur lors de la lecture du fichier XML : " + e.getMessage());
            System.exit(1);
        }
        return data;
    }

    private static Traitement readTreatmentFromFile(String filePath) {
        Traitement traitement = null;
        Map<String, Traitement> env = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            TraitementBuilder builder = new TraitementBuilder();
            while ((line = br.readLine()) != null) {
                Scanner scanner = new Scanner(line);
                traitement = builder.traitement(scanner, env);
            }
        } catch (IOException e) {
            System.err.println("Erreur lors de la lecture du fichier de traitement : " + e.getMessage());
            System.exit(1);
        }

        return traitement;
    }
}
