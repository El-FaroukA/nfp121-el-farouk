import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.Maj;
import org.nfp121.traitements.Position;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * MajTest
 */
public class MajTest extends TraitementTestAbstrait {

    private Maj maj;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Override
    protected Maj nouveauTraitement() {
        return new FabriqueTraitementConcrete().maj();
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.maj = nouveauTraitement();
        System.setOut(new PrintStream(outContent));
    }

    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testerMiseAJourNominal() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        this.maj.gererDebutLot("Lot1");

        this.maj.traiter(p1, 11.0);
        assertTrue(outContent.toString().contains("Mise à jour de " + p1 + " : 11.0"));
        outContent.reset();

        this.maj.traiter(p2, 22.0);
        assertTrue(outContent.toString().contains("Mise à jour de " + p2 + " : 22.0"));
        outContent.reset();

        this.maj.gererFinLot("Lot1");
        assertTrue(outContent.toString().contains("Lot1 : fin de mise à jour"));
    }
}
