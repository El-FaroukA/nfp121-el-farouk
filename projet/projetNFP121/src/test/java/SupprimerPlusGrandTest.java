import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.SupprimerPlusGrand;
import org.nfp121.traitements.Position;

import static org.junit.Assert.*;

/**
 * SupprimerPlusGrandTest
 */
public class SupprimerPlusGrandTest extends TraitementTestAbstrait {

    private SupprimerPlusGrand supprimerPlusGrand;
    private TraitementTestAbstrait.Dernier dernier;

    @Override
    protected SupprimerPlusGrand nouveauTraitement() {
        return new FabriqueTraitementConcrete().supprimerPlusGrand(15.0); // Seuil de 15.0
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.supprimerPlusGrand = nouveauTraitement();
        this.dernier = new TraitementTestAbstrait.Dernier();
        this.supprimerPlusGrand.ajouterSuivants(this.dernier);
    }

    @Test
    public void testerFiltrageNominal() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        this.supprimerPlusGrand.gererDebutLot("Lot1");

        // Valeurs inférieures ou égales au seuil
        this.supprimerPlusGrand.traiter(p1, 10.0);
        assertEquals(10.0, this.dernier.valeur, 1e-8);
        assertSame(p1, this.dernier.position);

        this.dernier.valeur = 0.0; // reset for next assertion

        this.supprimerPlusGrand.traiter(p2, 15.0);
        assertEquals(15.0, this.dernier.valeur, 1e-8);
        assertSame(p2, this.dernier.position);

        this.dernier.valeur = 0.0; // reset for next assertion

        // Valeur supérieure au seuil
        this.supprimerPlusGrand.traiter(p1, 20.0);
        assertEquals(0.0, this.dernier.valeur, 1e-8); // La valeur n'est pas transmise

        this.supprimerPlusGrand.gererFinLot("Lot1");
    }
}