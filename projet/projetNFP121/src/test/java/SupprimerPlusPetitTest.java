import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.traitements.SupprimerPlusPetit;
import org.nfp121.traitements.Position;

import static org.junit.Assert.*;

/**
 * SupprimerPlusPetitTest
 */
public class SupprimerPlusPetitTest extends TraitementTestAbstrait {

    private SupprimerPlusPetit supprimerPlusPetit;
    private TraitementTestAbstrait.Dernier dernier;

    @Override
    protected SupprimerPlusPetit nouveauTraitement() {
        return new SupprimerPlusPetit(15.0); // Seuil de 15.0
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.supprimerPlusPetit = nouveauTraitement();
        this.dernier = new TraitementTestAbstrait.Dernier();
        this.supprimerPlusPetit.ajouterSuivants(this.dernier);
    }

    @Test
    public void testerFiltrageNominal() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        // Appeler gererDebutLot avant de traiter les valeurs
        this.supprimerPlusPetit.gererDebutLot("Lot1");

        // Valeurs supérieures ou égales au seuil
        this.supprimerPlusPetit.traiter(p1, 20.0);
        assertEquals(20.0, this.dernier.valeur, 1e-8);
        assertSame(p1, this.dernier.position);

        this.dernier.valeur = 0.0; // reset for next assertion

        this.supprimerPlusPetit.traiter(p2, 15.0);
        assertEquals(15.0, this.dernier.valeur, 1e-8);
        assertSame(p2, this.dernier.position);

        this.dernier.valeur = 0.0; // reset for next assertion

        // Valeur inférieure au seuil
        this.supprimerPlusPetit.traiter(p1, 10.0);
        assertEquals(0.0, this.dernier.valeur, 1e-8); // La valeur n'est pas transmise

        // Appeler gererFinLot pour terminer le lot
        this.supprimerPlusPetit.gererFinLot("Lot1");
    }
}
