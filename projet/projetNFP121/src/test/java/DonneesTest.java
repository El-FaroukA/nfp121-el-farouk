import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.Donnees;
import org.nfp121.traitements.Position;

import static org.junit.Assert.*;

/**
 * DonneesTest
 *
 * @author
 */
public class DonneesTest extends TraitementTestAbstrait {

    private Donnees donnees;
    private static final double EPSILON = 1e-8;

    @Override
    protected Donnees nouveauTraitement() {
        return new FabriqueTraitementConcrete().donnees();
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.donnees = nouveauTraitement();
    }

    @Test
    public void testerEnregistrementDonnees() {
        Position p1 = new Position(7, 5);
        Position p2 = new Position(6, 2);
        this.donnees.gererDebutLot("Lot1");

        // Enregistrer les données
        this.donnees.traiter(p1, 45.0);
        this.donnees.traiter(p2, 8.5);

        // Vérifier les enregistrements
        assertEquals(2, this.donnees.getDonnees().size());
        assertTrue(this.donnees.getDonnees().containsKey(p1));
        assertTrue(this.donnees.getDonnees().containsKey(p2));
        assertEquals(45.0, this.donnees.getDonnees().get(p1), EPSILON);
        assertEquals(8.5, this.donnees.getDonnees().get(p2), EPSILON);
    }

    @Test
    public void testerNombreDonnees() {
        Position p1 = new Position(2, 0);
        Position p2 = new Position(9, 4);
        this.donnees.gererDebutLot("Lot1");

        // Enregistrer les données
        this.donnees.traiter(p1, 74.1);
        this.donnees.traiter(p2, 20.6);

        // Vérifier le nombre de données enregistrées
        assertEquals(2, this.donnees.getDonnees().size());
    }

    @Test
    public void testerPositionsEtValeurs() {
        Position p1 = new Position(1, 7);
        Position p2 = new Position(3, 4);
        this.donnees.gererDebutLot("Lot1");

        // Enregistrer les données
        this.donnees.traiter(p1, 4.6);
        this.donnees.traiter(p2, 11.6);

        // Vérifier les positions et valeurs enregistrées
        assertEquals(4.6, this.donnees.getDonnees().get(p1), EPSILON);
        assertEquals(11.6, this.donnees.getDonnees().get(p2), EPSILON);
    }
}
