import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.SommeParPosition;
import org.nfp121.traitements.Position;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * SommeParPositionTest
 */
public class SommeParPositionTest extends TraitementTestAbstrait {

    private SommeParPosition sommeParPosition;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Override
    protected SommeParPosition nouveauTraitement() {
        return new FabriqueTraitementConcrete().sommeParPosition();
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.sommeParPosition = nouveauTraitement();
        System.setOut(new PrintStream(outContent));
    }

    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testerSommeParPositionNominale() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        this.sommeParPosition.gererDebutLot("Lot1");

        this.sommeParPosition.traiter(p1, 10.0);
        this.sommeParPosition.traiter(p2, 20.0);
        this.sommeParPosition.traiter(p1, 5.0);

        this.sommeParPosition.gererFinLot("Lot1");

        String expectedOutput = "SommeParPosition Lot1 :\n" +
                "- " + p1 + " -> 15.0\n" +
                "- " + p2 + " -> 20.0\n" +
                "Fin SommeParPosition.\n";

        // Normalisation des séparateurs de lignes avant la comparaison
        String actualOutput = outContent.toString().replace("\r\n", "\n").replace("\r", "\n");
        String expectedNormalizedOutput = expectedOutput.replace("\r\n", "\n").replace("\r", "\n");

        assertEquals(expectedNormalizedOutput, actualOutput);
    }
}
