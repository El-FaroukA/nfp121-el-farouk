import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.Max;
import org.nfp121.traitements.Position;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * MaxTest
 */
public class MaxTest extends TraitementTestAbstrait {

    private Max max;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Override
    protected Max nouveauTraitement() {
        return new FabriqueTraitementConcrete().max();
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.max = nouveauTraitement();
        System.setOut(new PrintStream(outContent));
    }

    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testerMaxNominal() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        this.max.gererDebutLot("Lot1");

        this.max.traiter(p1, 11.0);
        this.max.traiter(p2, 22.0);

        this.max.gererFinLot("Lot1");
        assertTrue(outContent.toString().contains("Lot1 : max = 22.0"));
        outContent.reset();

        this.max.gererDebutLot("Lot2");
        this.max.traiter(p1, 33.0);
        this.max.traiter(p2, 5.0);

        this.max.gererFinLot("Lot2");
        assertTrue(outContent.toString().contains("Lot2 : max = 33.0"));
    }
}
