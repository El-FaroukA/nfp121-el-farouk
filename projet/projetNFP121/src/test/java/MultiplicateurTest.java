import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.Multiplicateur;
import org.nfp121.traitements.Position;

import static org.junit.Assert.*;

/**
  * MultiplicateurTest 
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class MultiplicateurTest extends TraitementTestAbstrait {

	private static final double EPSILON = 1e-8;

	private Multiplicateur multiplicateur;
	private TraitementTestAbstrait.Dernier dernier;

	@Override
	protected Multiplicateur nouveauTraitement() {
		return new FabriqueTraitementConcrete().multiplicateur(10.0);
	}

	@Override
	public void setUp() throws CycleException {
		super.setUp();
		this.multiplicateur = nouveauTraitement();
		this.dernier = new TraitementTestAbstrait.Dernier();
		this.multiplicateur.ajouterSuivants(this.dernier);
	}

	@Test
	public void testerMultiplicateurNominal() {
		Position p1 = new Position(1, 2);
		this.multiplicateur.gererDebutLot("Lot1");
		this.multiplicateur.traiter(p1, 11);
		assertEquals(110, this.dernier.valeur, EPSILON);
		assertSame(p1, this.dernier.position);

		this.multiplicateur.traiter(p1, 7);
		assertEquals(70, this.dernier.valeur, EPSILON);
		assertSame(p1, this.dernier.position);

		this.multiplicateur.gererFinLot("Lot1");
	}

}
