import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.Min;
import org.nfp121.traitements.Position;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * MinTest
 */
public class MinTest extends TraitementTestAbstrait {

    private Min min;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Override
    protected Min nouveauTraitement() {
        return new FabriqueTraitementConcrete().min();
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.min = nouveauTraitement();
        System.setOut(new PrintStream(outContent));
    }

    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testerMinNominal() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        this.min.gererDebutLot("Lot1");

        this.min.traiter(p1, 11.0);
        this.min.traiter(p2, 22.0);

        this.min.gererFinLot("Lot1");
        assertTrue(outContent.toString().contains("Lot1 : min = 11.0"));
        outContent.reset();

        this.min.gererDebutLot("Lot2");
        this.min.traiter(p1, 33.0);
        this.min.traiter(p2, 5.0);

        this.min.gererFinLot("Lot2");
        assertTrue(outContent.toString().contains("Lot2 : min = 5.0"));
    }
}
