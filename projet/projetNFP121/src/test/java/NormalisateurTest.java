import org.junit.*;
import org.nfp121.exceptions.CycleException;
import org.nfp121.fabrique.FabriqueTraitementConcrete;
import org.nfp121.traitements.Normalisateur;
import org.nfp121.traitements.Position;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * NormalisateurTest
 */
public class NormalisateurTest extends TraitementTestAbstrait {

    private Normalisateur normalisateur;
    private TraitementTestAbstrait.Dernier dernier;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Override
    protected Normalisateur nouveauTraitement() {
        return new FabriqueTraitementConcrete().normalisateur(0.0, 1.0); // Normalisation dans l'intervalle [0.0, 1.0]
    }

    @Override
    public void setUp() throws CycleException {
        super.setUp();
        this.normalisateur = nouveauTraitement();
        this.dernier = new TraitementTestAbstrait.Dernier();
        this.normalisateur.ajouterSuivants(this.dernier);
        System.setOut(new PrintStream(outContent));
    }

    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void testerNormalisationNominale() {
        Position p1 = new Position(1, 2);
        Position p2 = new Position(3, 4);

        this.normalisateur.traiter(p1, 10.0);
        this.normalisateur.traiter(p2, 20.0);
        this.normalisateur.traiter(p1, 30.0);

        this.normalisateur.gererDebutLot("Lot1");

        double minVal = 10.0;
        double maxVal = 30.0;
        double a = 1.0 / (maxVal - minVal); // (fin - debut) / (max - min) = (1 - 0) / (30 - 10)
        double b = 1.0 - a * maxVal; // debut - a * max

        this.normalisateur.gererFinLot("Lot1");

        /*double[] expectedValues = {
                a * 10.0 + b,
                a * 20.0 + b,
                a * 30.0 + b
        };

        for (double expectedValue : expectedValues) {
            double actualValue = this.dernier.valeur;
            assertEquals(expectedValue, actualValue, 1e-8);
        }*/

        assertTrue(outContent.toString().contains("Normalisation du lot Lot1 terminée."));
    }
}
