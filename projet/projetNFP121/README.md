# Analyseur de Données

Ce projet est un analyseur de données qui lit des données à partir d'un fichier XML, applique une série de traitements définis dans un fichier texte, puis génère les résultats en fonction des traitements spécifiés.

## Prérequis

- Java 17 ou supérieur
- Maven 3.6.0 ou supérieur

## Installation

1. Clonez le dépôt du projet :
    ```bash
    git clone https://gitlab.com/El-FaroukA/nfp121-el-farouk.git
    cd nfp121-el-farouk
    ```

2. Assurez-vous que vous avez Java et Maven installés en vérifiant les versions :
    ```bash
    java -version
    mvn -version
    ```

## Compilation

Pour compiler le projet et créer un JAR exécutable, exécutez les commandes suivantes :
```bash
mvn clean package / mvn clean install
```

Le fichier JAR exécutable sera généré dans le dossier `target` sous le nom `analyseurTraitement-1.0.jar`.

## Exécution

Pour exécuter le programme, utilisez la commande suivante :
```bash
java -jar analyseurTraitement-1.0.jar <chemin-vers-le-fichier-xml> <chemin-vers-le-fichier-de-traitement>
```

## Documentation

Voici un exemple de fichier du fichier XML contenant les positions à analyser :
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE donnees SYSTEM "donnees2.dtd">

<donnees>
    <donnee id="1" x="1" y="2">
        <valeur>15.0</valeur>
    </donnee>
    <donnee id="2" x="3" y="4">
        <valeur>20.0</valeur>
    </donnee>
    <donnee id="3" x="5" y="6">
        <valeur>25.0</valeur>
    </donnee>
    <donnee id="4" x="7" y="8">
        <valeur>30.0</valeur>
    </donnee>
</donnees>
```

Et voici un exemple de fichier de traitement contenant les traitements à appliquer :
```plaintext
Positions 0 1 Max 0 1 Somme 0 1 SommeParPosition 0
```

## Exemple

Des fichiers de données et de traitement sont fournis dans le dossier input se trouvant dans le dossier parent du projet.

