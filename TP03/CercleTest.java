import org.junit.*;
import static org.junit.Assert.*;

import java.awt.*;

public class CercleTest {

    public final static double EPSILON = 0.001;

    // Les points du sujet
    private Point point1, point2, point3, point4, point5;

    // Les cercles du sujet
    private Cercle cercle1, cercle2, cercle3;

    @Before
    public void setUp() {
        // Construire les points
        point1 = new Point(1, 2);
        point2 = new Point(2, 1);
        point3 = new Point(4, 1);
        point4 = new Point(8, 1);
        point5 = new Point(15, 4);

        // Construire les cercles
        cercle1 = new Cercle(point1, point2);
        cercle2 = new Cercle(point3, point4, Color.yellow);
        cercle3 = Cercle.creerCercle(point5, point1);
    }

    static void bonRayon(double rayon, double distance) {
        assertEquals("Rayon incorrect", rayon, distance, EPSILON);
    }

    static void bonneCouleur(Color couleur, Color couleurAttendue) {
        assertEquals("Couleur incorrecte", couleur, couleurAttendue);
    }

    @Test
    public void testerE12ColorIsBlue() {
        bonneCouleur(cercle1.getCouleur(), Color.blue);
    }

    @Test
    public void testerE12CenterIsPoint1() {
        assertEquals(cercle1.getCentre(), point1);
    }

    @Test
    public void testerE12RadiusIsDistanceBetweenPoint1AndPoint2() {
        bonRayon(cercle1.getRayon(), point1.distance(point2));
    }

    @Test
    public void testerE13ColorIsNotBlue() {
        assertNotEquals(cercle2.getCouleur(), Color.blue);
    }

    @Test
    public void testerE13ColorIsYellow() {
        assertEquals(cercle2.getCouleur(), Color.yellow);
    }

    @Test
    public void testerE13CenterIsPoint3() {
        assertEquals(cercle2.getCentre(), point3);
    }

    @Test
    public void testerE13RadiusIsDistanceBetweenPoint3AndPoint4() {
        assertEquals(cercle2.getRayon(), point3.distance(point4), EPSILON);
    }

    @Test
    public void testerE14ColorIsBlue() {
        assertEquals(cercle3.getCouleur(), Color.blue);
    }

    @Test
    public void testerE14CenterIsPoint5() {
        assertEquals(cercle3.getCentre(), point5);
    }

    @Test
    public void testerE14RadiusIsDistanceBetweenPoint5AndPoint1() {
        bonRayon(cercle3.getRayon(), point5.distance(point1));
    }
}