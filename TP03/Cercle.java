import java.awt.*;


public class Cercle implements Mesurable2D {
    private final Point centre; // centre du cercle
    private double rayon; // rayon du cercle
    private double diametre; // diametre du cercle
    private Color couleur = Color.blue; // couleur du cercle

    public final static double PI = Math.PI;


    // Constructeurs
    /**
     * Premier constructeur de la classe Cercle
     * @param centre : centre du cercle
     * @param rayon : rayon du cercle
     */
    public Cercle(Point centre, double rayon) {
        this.centre = centre;
        this.rayon = rayon;
        this.diametre = rayon * 2;
    }
    /**
     * Deuxième constructeur de la classe Cercle
     * @param point1 : le centre du cercle
     * @param point2 : un point du cercle
     */
    public Cercle (Point point1, Point point2) {
        this.centre = point1;
        this.rayon = point1.distance(point2);
        this.diametre = rayon * 2;
    }
    /**
     * Troisième constructeur de la classe Cercle
     * @param point1 : le centre du cercle
     * @param point2 : un point du cercle
     * @param couleur : la couleur du cercle
     */
    public Cercle (Point point1, Point point2, Color couleur) {
        this.centre = point1;
        this.rayon = point1.distance(point2);
        this.diametre = rayon * 2;
        this.couleur = couleur;
    }

    /**
     * Retourne le centre du cercle
     * @return le centre du cercle
     */
    public Point getCentre() {
        return centre;
    }
    /**
     * Retourne le rayon du cercle
     * @return le rayon du cercle
     */
    public double getRayon() {
        return rayon;
    }
    /**
     * Modifie le rayon du cercle
     * @param rayon : le nouveau rayon du cercle
     */
    public void setRayon(double rayon) {
        this.rayon = rayon;
    }
    /**
     * Retourne le diametre du cercle
     * @return le diametre du cercle
     */
    public double getDiametre() {
        return diametre;
    }
    /**
     * Modifie le diametre du cercle
     * @param diametre : le nouveau diametre du cercle
     */
    public void setDiametre(double diametre) {
        this.diametre = diametre;
        this.setRayon(diametre / 2);
    }
    /**
     * Retourne la couleur du cercle
     * @return la couleur du cercle
     */
    public Color getCouleur() {
        return couleur;
    }
    /**
     * Modifie la couleur du centre
     * @param couleur : la nouvelle couleur du centre
     */
    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }
    /**
     * Vérifie si un point est compris dans le cercle
     * @param point : le point à vérifier
     * @return true si le point est dans le cercle, false sinon
     */
    public boolean contient(Point point) {
        return this.getCentre().distance(point) <= this.getRayon();
    }
    /**
     * Translater le cercle
     * @param dx : déplacement suivant l'axe des X
     * @param dy : déplacement suivant l'axe des Y
     */
    public void translater(double dx, double dy) {
        this.getCentre().translater(dx, dy);
    }
    /**
     * Retourne le périmètre du cercle
     * @return le périmètre du cercle
     */
    @Override
    public double perimetre() {
        return 2 * PI * this.getRayon();
    }
    /**
     * Retourne l'aire du cercle
     * @return l'aire du cercle
     */
    @Override
    public double aire() {
        return Math.PI * Math.pow(this.getRayon(), 2);
    }
    /**
     * Permet la création d'un cercle à partir de deux points
     * @param point1 : le centre du cercle
     * @param point2 : un point du cercle
     * @return le cercle créé
     */
    public static Cercle creerCercle(Point point1, Point point2) {
        return new Cercle(point1, point2);
    }
    /**
     * Affiche les informations du cercle
     * @return les informations du cercle
     */
    public String toString() {
        return "C" + this.getRayon() + "@" + this.getCentre();
    }
}