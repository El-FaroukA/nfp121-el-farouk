import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListeurFabriquesStatiques implements Listeur {

    @Override
    public List<Method> getMethodes(String nomClasse) throws Exception {
        Class<?> c = Class.forName(nomClasse);
        return Stream.of(c.getDeclaredMethods())
                .filter(method -> Modifier.isStatic(method.getModifiers()) && method.getReturnType() == c)
                .filter(method -> Stream.of(method.getParameterTypes()).noneMatch(type -> type.getName().equals(c.getName())))
                .collect(Collectors.toList());
    }
}