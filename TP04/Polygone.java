import java.awt.Color;

/** Un Polygone est défini par la séquence de ses sommets.
 * On suppose que les polygones construits sont des polygones directs (les
 * sommets sont listés dans l'ordre anti-trigonométrique).
 * 
 * @author  Xavier Crégut
 * @version 1.10
 */
public class Polygone {

	private Point[] sommets;	// les sommets du polygone
	private int nbSommets;	// le nombre effectif de sommets
	private Color couleur;	// la couleur du polygone

	/** Construire un polygone sans sommets.
	 * @param max degré maximal du polygone
	 */
	public Polygone(int max) {
		this.sommets = new Point[max];
		this.nbSommets = 0;
		this.couleur = Color.green;
	}

	/** Obtenir le nb maximal de sommets du polygone.
	 *  @return le nb maximal de sommets du polygone
	 */
	public int getDegreMax() {
		return this.sommets.length;
	}

	/** Obtenir le nb de sommets du polygone.
	 *  @return le nb de sommets du polygone
	 */
	public int getDegre() {
		return this.nbSommets;
	}

	/**  Ajouter le sommet p à la position précisée.
	 *  @param p le sommet à ajouter
	 *  @param position la position d'insertion
	 */
	public void ajouter(Point p, int position) {
		// Décaler les sommets après position
		for (int i = this.getDegre(); i >= position; i--) {
			this.sommets[i] = this.sommets[i-1];
		}

		// Mettre p à sa position
		this.sommets[position-1] = p;

		// Incrémenter le nb de sommets
		this.nbSommets++;
	}

	/** Ajouter le sommet p en dernière position.
	 * @param p le sommet à ajouter
	 */
	public void ajouter(Point p) {
		this.sommets[this.nbSommets++] = p;
	}

	/**
	 *  Supprimer un sommet du polygone
	 *  @param position la position du sommet à supprimer
	 */
	public void supprimer(int position) {
		// Décaler les sommets
		for (int i = position; i < this.getDegre(); i++) {
			this.sommets[i-1] = this.sommets[i];
		}

		// Décrémenter le nombre de sommets
		this.nbSommets--;

		this.sommets[nbSommets] = null;	// forcer l'oubli du Point.
	}

	/**  Obtenir le i<sup>è</sup> sommet du polygone.
	 * @param i le numéro du sommet compris entre 0 et nbSommets - 1
	 * @return le i<sup>è</sup> sommet du polygone
	 */
	public Point getSommet(int i) {
		return this.sommets[i-1];
	}

   /** Translater le polygone.
	 * @param dx déplacement suivant l'axe des X
	 * @param dy déplacement suivant l'axe des Y
	 */ 
	public void translater(double dx, double dy) {
		for (int i = 1; i <= this.getDegre(); i++) {
			this.getSommet(i).translater(dx, dy);
		}
	}

	/** Afficher le polygone.  Le polygone est affiché sous la forme :
	  * <PRE>
	  *		<< extremite1 extremite2 .. extremiteN >>
	  * </PRE>
	  */
	public void afficher() {
		System.out.print("<< ");
		for (int i = 1; i <= this.getDegre(); i++) {
			this.getSommet(i).afficher();
			System.out.print(" ");
		}
		System.out.print(">>");
	}

	/** Dessiner le polygone sur l'afficheur.
	 * @param afficheur l'afficheur à utiliser
	 */
	public void dessiner(afficheur.Afficheur ecran) {
		if (this.getDegre() > 0) {
			for (int i = 1; i < this.getDegre(); i++) {
				ecran.dessinerLigne(
						this.getSommet(i).getX(), this.getSommet(i).getY(),
						this.getSommet(i+1).getX(), this.getSommet(i+1).getY(),
						this.getCouleur());
			}
			ecran.dessinerLigne(this.getSommet(getDegre()).getX(),
						this.getSommet(getDegre()).getY(),
						this.getSommet(1).getX(),
						this.getSommet(1).getY(), this.getCouleur());
		}
	}

	/**  Le périmètre du polygone.  */
	public double getPerimetre() {
		double result = 0;
		if (this.getDegre() >= 2) {
			result = this.getSommet(1).distance(
						this.getSommet(this.getDegre()));
			if (this.getDegre() >= 3) {
				for (int i = 1; i < this.getDegre(); i++) {
					result += this.getSommet(i).distance(this.getSommet(i+1));
				}
			}
		}
		return result;
	}

	/**  La surface du polygone.  */
	public double getSurface() {
		double result = 0;
		if (this.getDegre() > 2) {
			for (int i = 1; i < getDegre(); i++) {
				Point p1 = this.getSommet(i);
				Point p2 = this.getSommet(i+1);
				result += (p2.getX() - p1.getX()) * (p1.getY() + p2.getY()) / 2;
			}
			Point p1 = this.getSommet(getDegre());
			Point p2 = this.getSommet(1);
			result += (p2.getX() - p1.getX()) * (p1.getY() + p2.getY()) / 2;
		}
		return Math.abs(result);
	}

	/** Couleur du polygone. */
	public Color getCouleur() {
		return this.couleur;
	}

	/** Changer la couleur du polygone.
	 * @param nouvelleCouleur nouvelle couleur
	 */
	public void setCouleur(Color nouvelleCouleur) {
		this.couleur = nouvelleCouleur;
	}

}
