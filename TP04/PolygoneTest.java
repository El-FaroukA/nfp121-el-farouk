/** Test de la classe Polygone.
  *
  * @author	Xavier Crégut
  * @version	1.2
  */
public class PolygoneTest extends junit.framework.TestCase {

	public final static double EPSILON = 0.0001;

	private Polygone p0;
	private Polygone p1;
	private Point s1, s2, s3, s4;

	protected void setUp() {
		p0 = new Polygone(4);

		p1 = new Polygone(5);
		p1.ajouter(s1 = new Point(0, 0));
		p1.ajouter(s3 = new Point(5, 5));
		p1.ajouter(s4 = new Point(5, 0));
		p1.ajouter(s2 = new Point(0, 5), 2);
	}

	public void testInitialisation() {
		assertNotNull(p0);
		assertNotNull(p1);
		assertEquals(0, p0.getDegre());
		assertEquals(4, p1.getDegre());
	}

	public void testInitialisation2() {
		assertEquals(s1, p1.getSommet(1));
		assertEquals(s2, p1.getSommet(2));
		assertEquals(s3, p1.getSommet(3));
		assertEquals(s4, p1.getSommet(4));
	}

	public void testDimensions() {
		assertEquals(20, p1.getPerimetre(), EPSILON);
		assertEquals(25, p1.getSurface(), EPSILON);
	}

	public void testTranslater() {
		p1.translater(20, 10);
		assertEquals(20, p1.getSommet(1).getX(), EPSILON);
		assertEquals(10, p1.getSommet(1).getY(), EPSILON);
		assertEquals(25, p1.getSommet(4).getX(), EPSILON);
		assertEquals(10, p1.getSommet(4).getY(), EPSILON);
		assertEquals(20, p1.getPerimetre(), EPSILON);
		assertEquals(25, p1.getSurface(), EPSILON);
	}

	public void testSupprimer() {
		p1.supprimer(2);
		assertEquals(10 + Math.sqrt(50), p1.getPerimetre(), EPSILON);
		assertEquals(12.5, p1.getSurface(), EPSILON);
		assertEquals(3, p1.getDegre());
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(
				new junit.framework.TestSuite(PolygoneTest.class));
	}

}
