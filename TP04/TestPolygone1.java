/** Programme de test du Polygone.
  * @author	Xavier Crégut
  * @version	1.2
  */
public class TestPolygone1 {

	public static void main(String[] args) {
		// Construire un polygone (carré de côté 5).
		Polygone p1 = new Polygone(5);
		p1.ajouter(new Point(0, 0));
		p1.ajouter(new Point(5, 5));
		p1.ajouter(new Point(5, 0));
		p1.ajouter(new Point(0, 5), 2);

		// Afficher le polygone
		p1.afficher();	System.out.println();

		// Afficher les caractéristiques du polygone
		System.out.println("Périmètre = " + p1.getPerimetre());
		System.out.println("Surface   = " + p1.getSurface());

		// Construire un afficheur
		afficheur.Ecran ecran
				= new afficheur.Ecran("Test Polygone", 400, 400, 10);
		ecran.dessinerAxes();

		// Dessiner l'écran
		p1.dessiner(ecran);

		// Translater le polygone de (10, 15);
		p1.translater(20, 10);

		// Changer la couleur du polygone
		p1.setCouleur(java.awt.Color.blue);

		// Afficher le polygone
		p1.afficher();	System.out.println();

		// Dessiner l'écran
		p1.dessiner(ecran);
	}

}
