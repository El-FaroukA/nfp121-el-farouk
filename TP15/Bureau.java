import java.util.List;

public class Bureau {
    private String name;
    private List<Personnel> personnels;

    public Bureau(String name, List<Personnel> personnels) {
        this.name = name;
        this.personnels = personnels;
    }

    public String getName() {
        return name;
    }

    public List<Personnel> getPersonnels() {
        return personnels;
    }

    @Override
    public String toString() {
        return "Bureau{" +
                "name='" + name + '\'' +
                ", personnels=" + personnels +
                '}';
    }

    public void addPersonnel(Personnel personnel) {
        personnels.add(personnel);
    }

    public void removePersonnel(Personnel personnel) {
        personnels.remove(personnel);
    }

    public void clearPersonnels() {
        personnels.clear();
    }

    public int getPersonnelCount() {
        return personnels.size();
    }

    public Personnel getPersonnel(int index) {
        return personnels.get(index);
    }

    public Personnel getPersonnel(String name) {
        for (Personnel personnel : personnels) {
            if (personnel.getName().equals(name)) {
                return personnel;
            }
        }
        return null;
    }

    public boolean hasPersonnel(String name) {
        for (Personnel personnel : personnels) {
            if (personnel.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasPersonnel(Personnel personnel) {
        return personnels.contains(personnel);
    }

}
