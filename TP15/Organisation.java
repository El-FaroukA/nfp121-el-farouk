import java.util.Map;

public class Organisation {
    private Map<Personnel, Bureau> employees;

    public Organisation(Map<Personnel, Bureau> employees) {
        this.employees = employees;
    }

    public Map<Personnel, Bureau> getEmployees() {
        return employees;
    }

    public void addEmployee(Personnel personnel, Bureau bureau) {
        bureau.addPersonnel(personnel);
        employees.put(personnel, bureau);
    }

    public void removeEmployee(Personnel personnel) {
        Bureau bureau = employees.get(personnel);
        if (bureau != null) {
            bureau.removePersonnel(personnel);
            employees.remove(personnel);
        }
    }

    public void clearEmployees() {
        employees.clear();
    }

    public int getEmployeeCount() {
        return employees.size();
    }

}
