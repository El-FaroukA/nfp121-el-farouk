public class Personnel {
    private String name;

    public Personnel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
